// Users
{
	"id": 1,
	"firstName": "John",
	"lastName": "Doe",
	"email": "johndoe@mail.com",
	"password": "john1234",
	"isAdmin": false,
	"mobileNo": "09237593671"
}
{
	"id": 2,
	"firstName": "EJ",
	"lastName": "Urbina",
	"email": "ejnurbina@gmail.com",
	"password": "4321je",
	"isAdmin": false,
	"mobileNo": "09996658925"
}

// Orders
{
	"id": 10,
	"userId": 1,
	"productID" : 25,
	"transactionDate": "08-15-2021",
	"status": "paid",
	"total": 1500
}
{
	"id": 11,
	"userId": 2,
	"productID" : 22,
	"transactionDate": "11-15-2022",
	"status": "paid",
	"total": 10500
}

// Products
{
	"id": 25,
	"name": "Humidifier",
	"description": "Make your home smell fresh any time.",
	"price": 300,
	"stocks": 1286,
	"isActive": true,
}

{
	"id": 22,
	"name": "GeForce GTX 1660 Super",
	"description": "mid-range graphics card by NVIDIA.",
	"price": 10500,
	"stocks": 11,
	"isActive": true,
}
